import pandas as pd 
import re
import os
import snakemake
from snakemake.io import expand, multiext, glob_wildcards, directory
from Bio import SearchIO
from snakemake.utils import validate

configfile: "config.yaml"

# Globals ---------------------------------------------------------------------

databases = config["databases"]  
  
 

###Indices for the fasta splited files, in the form of '001', '002'...
my_indices=[]
fasta_split_number=config["fasta_split_number"]
for x in range(1,fasta_split_number+1): ##Because the last value is exclude. If it's a 4, the range will stop to 3.
    my_indices.append('%03d' % x)

input=config["input"]


wildcard_constraints:
    accession= "|".join(input.keys())

rule all:
    input:
        expand("results/{accession}.out", accession=input.keys()), 
        #expand("results/{accession}.html", accession=input.keys())

# Split fasta in n pieces
rule split:
    input:
        fasta = lambda wildcards: input[wildcards.accession]["fasta"]
    output:
        temp(expand("temp/{{accession}}/{{accession}}.part_{n}.fasta", n=my_indices))
    params:
        number_of_output_fasta=fasta_split_number
    resources:
        partition='agap_short'
    envmodules:
        config["modules"]["seqkit"]
    singularity:
        config["containers"]["seqkit"]
    shell:
        "seqkit split --out-dir temp/{wildcards.accession} --by-part {params.number_of_output_fasta} {input}"


### Run blastn 
 
rule blast:
    input:
        fasta_files="temp/{accession}/{accession}.part_{indice}.fasta",
        databases=config["databases"]+".nal"
    output:
        temp("temp/{accession}/{accession}.part_{indice}.out")
    params:
        db = config["databases"],
        advanced_param = config["blast_params"]
    envmodules:
        config["modules"]["blast"]
    singularity:
        config["containers"]["blast"]
    threads:
        config["threads"]
    shell:"""
        blastn -db {params.db} -query {input.fasta_files} -out {output} {params.advanced_param}
    """

# Merge blast result
rule merge_blast_results:
    input:
        expand("temp/{accession}/{accession}.part_{indice}.out", accession=input.keys(),indice=my_indices)
    output:
        "results/{accession}.out"
    shell:
        "cat {input} > {output}"
 
rule blast2html:
    input:
        "results/{accession}.out"
    output:
        "results/{accession}.html"
    envmodules:
        config["modules"]["perl"]
    singularity:
        config["containers"]["perl"]
    shell:"""
        scripts/ImportBLAST.pl {input} -o {output}
    """