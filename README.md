# Snakemake workflow: Optimize blast execution

[![Snakemake](https://img.shields.io/badge/snakemake-≥6.3.0-brightgreen.svg)](https://snakemake.github.io)           

**Table of Contents**

  - [Objective](#objective)
  - [Dependencies](#dependencies)
  - [Prerequisites](#prerequisites)
  - [Procedure](#procedure)
  - [Results](#results)
  - [Authors](#authors)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objective

Split a FASTA file into chunks and executes a BLAST query for each chunk in parallel.

## Dependencies

* seqkit : SeqKit - a cross-platform and ultrafast toolkit for FASTA/Q file manipulation (https://bioinf.shenwei.me/seqkit/)
* ncbi-blast : Basic Local Alignment Search Tool (https://blast.ncbi.nlm.nih.gov/Blast.cgi)


## Prerequisites

- Globally installed SLURM 18.08.7.1+
- Globally installed singularity 3.4.1+
- Installed Snakemake 6.0.0+ (<8.0)

## Procedure 

- Clone the code repository from github to your computer by running the
  following shell command in your terminal:
  
```bash
git clone https://gitlab.cirad.fr/umr_agap/workflows/snakeblast.git
```


- Change directories into the cloned repository:

```bash
cd snakeblast
```

- Edit the configuration file config.yaml

```bash
#input:
#  "accession":
#    "fasta": "<path_to_fasta>"

input:
  "AOLQ-368":
    "fasta": "data/AOLQ-368.fasta" 
 
databases: "/gpfs1/agap/BANK/biomaj/nt/current/flat/nt" 
threads: 4
   
blast_params: " -strand both -evalue 0.001 -max_target_seqs 1 -outfmt 6 -dust yes -task blastn"

# Split FASTA into 20 files (increase/decrease this value if need, 5000 sequence for each part is recommended) 
fasta_split_number: 20

containers:
  blast: "https://depot.galaxyproject.org/singularity/blast%3A2.15.0--pl5321h6f7f691_1"
  seqkit: "https://depot.galaxyproject.org/singularity/seqkit%3A2.7.0--h9ee0642_0"
   
```

- Print shell commands to validate your modification (mode dry-run)

```bash
sbatch snakemake dry
```

- Run workflow on Meso@LR

```bash
sbatch snakemake.sh run
```

- Unlock directory

```bash
sbatch snakemake.sh unlock
```

- Generate DAG file

```bash
sbatch snakemake.sh dag
```

## Results : 

- results/\<accession\>.out
 
## Authors

- Gaetan Droc (CIRAD)
