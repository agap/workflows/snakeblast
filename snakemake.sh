#!/bin/bash 
#
#SBATCH -J blastn
#SBATCH -o logs/blastn."%j".out
#SBATCH -e logs/blastn."%j".err 

# Partition name
#SBATCH -p agap_long
 

module load snakemake/7.15.1-conda
module load singularity/3.5

mkdir -p logs/

### Snakemake commands
# Dry run
if [ "$1" = "dry" ]
then
    snakemake --profile profile  --jobs 200 --printshellcmds --dryrun --use-envmodules --cores 1
elif [ "$1" = "unlock" ]
then
    # Unlock repository if one job failed
    snakemake   --profile profile --jobs 200 --unlock  --use-envmodules
elif [ "$1" = "dag" ]
then
    # Create DAG file
    snakemake  --profile profile --jobs 2 --dag  --use-envmodules | dot -Tpng > images/dag.png
else
    # Run workflow
    snakemake --profile profile --jobs 60 --cores 140 -p --use-envmodules --latency-wait 120
fi